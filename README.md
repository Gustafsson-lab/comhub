# ComHub
ComHub is a Python software to infer hubs of gene regulatory networks. ComHub makes a community prediction of hubs by averaging over predictions by a compendium of network inference methods. ComHub either takes a set of GRN predictions as input or ComHub uses a set of network inference methods to infer GRNs from gene expression data. ComHub has 6 network inference methods implemented: "trustful inference of gene regulation with stability selection" (TIGRESS) (Haury et al. 2012), bootstrap ElasticNet (Zou et al. 2005), "gene network inference with ensemble of trees" (GENIE3) (Huynh-Thu et al. 2010), absolute pearson correlation coefficient (Butte et al. 2000), "algorithm for the reconstruction of accurate cellular networks" (ARACNE) (Margolin et al. 2006) and "context likelihood of relatedness" (CLR) (Faith et al. 2007). The network inference methods takes two text files as input, one containing the gene expression data and one with possible regulators. The output of ComHub is a list with regulators ranked on outdegree.

## Installation
See complete installation instructions in [doc/installation.md](https://gitlab.com/Gustafsson-lab/comhub/tree/master/doc/). 

### Minimum dependencies
- Python3
- numpy
- pandas
- scipy
- matplotlib
- seaborn

### Additional dependencies to use all network inference methods
- scikit-learn
- joblib
- R
- rpy2
- minet
- GNU Octave*
- oct2py*
- MATLAB*  
*MATLAB and Octave are optional to install. Not needed if running the R versions of the methods TIGRESS and CLR.

## Demo usage
Install comhub as stated in [doc/installation.md](https://gitlab.com/Gustafsson-lab/comhub/tree/master/doc/).  
Start python and place yourself in comhub folder.  
Run comhub on test data:
```
from bin.comhub import comhub
c = comhub(network_name='test')
c.run_methods()
edge_cutoff = c.pairwise_correlation()
tf_outdegree = c.get_tf_outdegree(edge_cutoff)
community = c.community(tf_outdegree)
```
The results are saved in the folder "/path/to/comhub/results/test/".

## Documentation
Documentation is available in [doc/documentation.md](https://gitlab.com/Gustafsson-lab/comhub/tree/master/doc/).  
Some documentation is available using:
```
c.{function}.__doc__
```

## License
Copyright 2019 Julia Åkesson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## References
- Butte,A.J. and Kohane,I.S. (2000) Mutual Information Relevance Networks: Functional Genomic Clustering Using Pairwise Entropy Measurements. Pacific Symp. Biocomput., 5, 415--426.
- Faith,J.J. et al. (2007) Large-scale mapping and validation of Escherichia coli transcriptional regulation from a compendium of expression profiles. PLoS Biol., 5, 0054--0066.
- Haury,A.C. et al. (2012) TIGRESS: Trustful Inference of Gene REgulation using Stability Selection. BMC Syst. Biol., 6, 1--17.
- Huynh-Thu,V.A. et al. (2010) Inferring regulatory networks from expression data using tree-based methods. PLoS One, 5, 1--10.
- Margolin,A.A. et al. (2006) ARACNE: An algorithm for the reconstruction of gene regulatory networks in a mammalian cellular context. BMC Bioinformatics, 7(Suppl1), 1--15.
- Zou,H. and Hastie,T. (2005) Regularization and variable selection via the elastic-net. J. R. Stat. Soc., 67, 301--320.
