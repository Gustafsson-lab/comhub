function [edges] = tigress_octave(comhubpath,networkname,expression_data,transcription_factors,network_cutoff)
%Read data
fprintf('Getting expression data ... \n')
A=importdata(expression_data,'\t');
%% Set data structure
data.expdata=A.data;
genenames=strsplit(A.textdata{1,1},'\t'); 
data.genenames=genenames;
fprintf('Getting transcription factors ... \n')
tflist=importdata(transcription_factors,'\t');
data.tflist=tflist;
data.tf_index=find(ismember(data.tflist,data.genenames));
%% Get frequency matrix F
freq=tigress(data);

%% Get scores
scores=score_edges(freq);

%% Write edges
network_file=[comhubpath,'/networks/',networkname,'/tigress_network.tsv']
if isempty(network_cutoff)
    edges = predict_network(scores,data.tf_index,'genenames',data.genenames,'name_net',network_file);%
else
    edges = predict_network(scores,data.tf_index,'genenames',data.genenames,'name_net',network_file,'cutoff', str2num(network_cutoff));
end

end
