function [edges] = tigress_matlab(comhubpath,networkname,expression_data,transcription_factors,network_cutoff)
%Read data
fprintf('Getting expression data ... \n')
A=importdata(expression_data);
%% Set data structure
data.expdata=A.data;
data.genenames=A.textdata';
fprintf('Getting transcription factors ... \n')
tflist=importdata(transcription_factors);
data.tflist=tflist;
data.tf_index=find(ismember(data.tflist,data.genenames));

%% Get frequency matrix F
freq=tigress(data);

%% Get scores
scores=score_edges(freq);

%% Write edges
network_file=[comhubpath,'/networks/',networkname,'/tigress_network.tsv']
edges = predict_network(scores,data.tf_index,'genenames',data.genenames,'name_net',network_file,'cutoff',str2num(network_cutoff));%
end
