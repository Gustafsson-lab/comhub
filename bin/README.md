# ComHub methods source code
Some of the implemented network inference methods uses orginal versions or modified versions of the source code. ComHub is built to work with the versions available as part of ComHub. No guarantees are made that ComHub can be used with any other versions of the source code.

## CLR
- The "clr.m" script were downloaded as part of the CLR algorithm from http://m3d.mssm.edu/network_inference.html [06/08/2018].
- "clr_octave.m" is a slightly modified version of "clr.m" to work in octave.

## GENIE3
- The "GENIE3.py" script were downloaded from https://github.com/vahuynh/GENIE3 [13/10/2018].

## TIGRESS
- TIGRESS version 2.1 were downloaded from http://projets.cbio.mines-paristech.fr/~ahaury/svn/dream5/html/index.html [06/08/2018].
- TIGRESS were slightly modified to fit the format of ComHub. All modifications are in "tigress/tigress_matlab.m" and "tigress/tigress_octave.m".
