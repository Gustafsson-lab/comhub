# Data format
ComHub requires two input files:
- Expression data (data/{network_name}_expression_data.tsv)
- A list of regulators (data/{network_name}_transcription_factors.tsv)

The Expression data need to be in the format
```
gene1	gene2	gene3
0.62	0.25	0.35
0.51	0.21	0.40
0.46	0.15	0.32
```
The regulator file need to be in the format:
```
gene1
gene2
gene3
```

It doesn't matter which type of gene names that are used as long as all genes listed in the regulator file are included in the gene names of the expression data file.  

I have provided a list of possible human transciption factors (Gene Symbols) from 
[Lambert,S.A. et al. (2018) The Human Transcription Factors. Cell, 172, 650–665.](http://humantfs.ccbr.utoronto.ca/). However, to currently use this file it needs to be changed for the expression data in question, so it only contains regulators that are also present in the expression data.
