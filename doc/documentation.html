<h1 id="comhub-documentation">ComHub Documentation</h1>
<h2 id="table-of-contents">Table of Contents</h2>
<ul>
<li><a href="#usage">Usage</a>
<ul>
<li><a href="#demo-usage">Demo usage</a></li>
<li><a href="#available-comhub-methods">Available ComHub methods</a>
<ul>
<li><a href="#setup-comhub">Setup ComHub</a></li>
<li><a href="#infer-gene-regulatory-networks">Infer gene regulatory networks</a></li>
<li><a href="#search-for-a-edge-threshold">Search for a edge threshold</a></li>
<li><a href="#making-a-community-hub-prediction">Making a community hub prediction</a></li>
<li><a href="#identifying-hubs">Identifying hubs</a></li>
<li><a href="#performance-assessment">Performance assessment</a></li>
<li><a href="#documentation">Documentation</a></li>
</ul></li>
</ul></li>
<li><a href="#license">License</a></li>
<li><a href="#references">References</a></li>
</ul>
<h2 id="usage">Usage</h2>
<h3 id="demo-usage">Demo usage</h3>
<p>Install comhub as stated in <a href="https://gitlab.com/Gustafsson-lab/comhub/tree/master/doc/">installation instructions</a>. Place yourself in comhub folder and start Python.</p>
<p>Run comhub on test data:</p>
<pre><code>from bin.comhub import comhub
c = comhub(network_name=&#39;test&#39;)
c.run_methods(bootstrap=10) #To reduce time of Elastic Net bootstrap is set to 10.
edge_cutoff = c.pairwise_correlation()
tf_outdegree = c.get_tf_outdegree(edge_cutoff)
community = c.community(tf_outdegree)</code></pre>
<p>The results are saved in the folder “/path/to/comhub/results/test/”.</p>
<h3 id="available-comhub-methods">Available ComHub methods</h3>
<p>ComHub is implemented as a Python class. ComHub requires two input files containing gene expression data and a list of possible transcription factors.</p>
<h4 id="setup-comhub">Setup comhub</h4>
<p>Import ComHub with:</p>
<pre><code>from bin.comhub import comhub</code></pre>
<p>Create a ComHub object with:</p>
<pre><code>c = comhub(network_name=&#39;test&#39;, methods=[&#39;aracne&#39;, &#39;clr_R&#39;, &#39;pcc&#39;, &#39;elasticnet_bootstrap&#39;, &#39;tigress_R&#39;, &#39;genie3&#39;], expression_data=None, transcription_factors=None, gold_standard=None)</code></pre>
<p>By default ComHub inputs expression data named "/path/to/comhub/data/{network_name}_expression_data.tsv" and possible regulators named "/path/to/comhub/data/{network_name}_transcription_factors.tsv". It is possible to submit any files with expression data and transcription factors. The expression data need to be tab seperated where columns are genes and rows are samples. There should be column names but no row names. Example of expression data format:</p>
<pre><code>gene1   gene2   gene3
0.62    0.25    0.35
0.51    0.21    0.40
0.46    0.15    0.32</code></pre>
<p>Example of transcription factors format:</p>
<pre><code>tf1
tf2
tf3</code></pre>
<p>The parameter methods is used to set which network inference methods that should be used for predicting hubs. By default aracne, clr_R, pcc, elasticnet_bootstrap, tigress_R, and genie3 are used. It is possible to provide networks inferred by your own methods. Then you set the parameter methods to a list with the methods you want to use, including the name of your method.</p>
<h4 id="infer-gene-regulatory-networks">Infer gene regulatory networks</h4>
<p>The network inference methods are applied on the ComHub object. All predicted networks will be saved as "/path/to/comhub/networks/{network_name}/{method}_network.csv". Run all network inference methods stated when making the ComHub object with:</p>
<pre><code>c.run_methods(network_cutoff=100000, nstepsLARS=5, matlab=False, bootstrap=100, parallel=True)</code></pre>
<p>This is only recommended on very small data sets. For most data sets it’s better to run the methods one by one.</p>
<p>It is possible to run all network inference methods separately with:</p>
<pre><code>net = c.aracne(network_cutoff=100000)   
net = c.pcc(network_cutoff=100000)
net = c.genie3(network_cutoff=100000)</code></pre>
<p>CLR and TIGRESS are both available as a R-version and a MATLAB-version.<br />
Run CLR and TIGRESS in R with:</p>
<pre><code>net = c.clr_R(network_cutoff=100000)
net = c.tigress_R(network_cutoff=100000, nstepsLARS=5, alpha=0.2, nsplit=100)</code></pre>
<p>For the MATLAB-version of CLR and TIGRESS it is possible to choose if the method should be run in MATLAB or Octave. By default the methods will run in Octave. However, for faster performance it is recommended to run in MATLAB.</p>
<pre><code>net = c.clr(network_cutoff=100000, matlab=False)
net = c.tigress(network_cutoff=100000, matlab=False)</code></pre>
<p>For bootstrap Elastic Net it is posible to choose how many bootstraps that should be run and if the bootstraps should be run in parallell.</p>
<pre><code>net = c.elasticnet_bootstrap(bootstrap=100, cv=3, network_cutoff=100000, large_dataset=False, matrix_name=&#39;&#39;)</code></pre>
<p>For large data sets, you might prefer to do several runs of Elastic net only with one or a few bootstraps and save the resulting matrices. When all runs are done you can combine the produced matrices into a network.</p>
<p>Running ElasticNet with large_dataset=True will save the resulting matrix into the folder “/path/to/comhub/networks/{network_name}/en_matrices”. The method elasticnet_combine_matrices() can then be used to combine all the matrices saved in that folder. You can also submit all the filepaths in a list.</p>
<pre><code>for i in range(100):
    c.elasticnet_bootstrap(bootstrap=1, large_dataset=True, matrix_name=str(i))
net = c.elasticnet_combine_matrices(files=None, network_cutoff=100000)</code></pre>
<p>The last method implemented in ComHub is bootstrap LASSO, which can be run in the same way as Elastic Net. For smaller data sets use:</p>
<pre><code>net = c.elasticnet_bootstrap(bootstrap=100, cv=3, network_cutoff=100000, large_dataset=False, matrix_name=&#39;&#39;)</code></pre>
<p>For large data sets you can use:</p>
<pre><code>for i in range(100):
    c.elasticnet_bootstrap(bootstrap=1, large_dataset=True, matrix_name=str(i))
net = c.elasticnet_combine_matrices(files=None, network_cutoff=100000)</code></pre>
<h4 id="search-for-a-edge-threshold">Search for a edge threshold</h4>
<p>An edge threshold is identified using the method pairwise_correlation(). pairwise_correlation() reads predicted networks from the network inference methods stated when making the comhub object. pairwise_correlation() looks after files named "/path/to/comhub/networks/{network_name}/{method}_network.tsv“. The function will generate a figure if plot=True that will be saved as”/path/to/comhub/results/{network_name}/pairwise_correlation.png". It is possible to generate several figures for different edge ranges by setting fig_name=‘your_name’ that will be added to the figure name as pairwise_correlation{your_name}.png.</p>
<pre><code>edge_cutoff = c.pairwise_correlation(edge_range=[500, 1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000], plot=True, fig_name=&#39;&#39;, network_files=None)</code></pre>
<p>The parameter edge range sets which edge thresholds to run pairwise correlation for. The method will output the optimal one of these edge thresholds. Using the parameter network_files you can input a list with filepaths to any network files you want to use. By default the network files "/path/to/comhub/networks/{network_name}/{method}_network.tsv" will be used.</p>
<h4 id="making-a-community-hub-prediction">Making a community hub prediction</h4>
<p>The regulator outdegrees of each predicted network is calcualted using the method get_tf_outdegree(). By default get_tf_outdegree() inputs predicted networks from the network inference methods stated when making the comhub object. get_tf_outdegree() looks in the folder “/path/to/comhub/networks/{network_name}/” after predicted networks named as "{method}_network.csv". It is possible to input your own network files as a list by setting network_files=[‘/path/to/network1’,‘path/to/network2’,…,‘/path/to/networkn’]</p>
<pre><code>tf_outdegree = c.get_tf_outdegree(edge_cutoff=100000, network_files=None)</code></pre>
<p>The community regulator outdegree is calculated using community(). community() saves the result as “/path/to/comhub/results/{network_name}/community.csv”. It is possible to add an additional name to the output with output_name=‘your_name’, to allow communities being saved for different edge thresholds. community() outputs a list of regulators ranked on outdegree.</p>
<pre><code>community = c.community(tf_outdegree, save_csv=True, output_name=&#39;&#39;)</code></pre>
<h4 id="identifying-hubs">Identifying hubs</h4>
<p>Hubs can be identified from the community using the method hubs(). hubs() by default identifies the regulators standing for 10% of the total interactions in the network. hubs() saves the result as “/path/to/comhub/results/{network_name}/hubs.csv”. It is possible to add an additional name to the output with output_name=‘your_name’.</p>
<pre><code>hubs = c.hubs(community, percentage_interactions=0.1, save_csv=True, output_name=&#39;&#39;)</code></pre>
<h4 id="performance-assessment">Performance assessment</h4>
<p>It is possible to assess the performance of ComHub and the network inference methods if a gold standard network, named "/path/to/comhub/data/{network_name}_gold_network.tsv", is available. The file path to a gold standard can also be provided when creating the ComHub object by changing the parameter gold_standard.<br />
The performance of ComHub is calculated using the method community_performance(). community_performance() calculates the pearson correlation coefficient between regulator outdegrees in ComHub prediction and in the gold standard.</p>
<pre><code>pcc, pval = c.community_performance(community)</code></pre>
<p>The performance of ComHub compared to the network inference methods is calculated using the method method_performance(). The function can be run for different edge thresholds. The result is saved as a figure named “/path/to/comhub/results/{network_name}/method_performance_{edge_threshold}.png”.</p>
<pre><code>performance = c.method_performance(edge_cutoff, plot=True)</code></pre>
<p>The average performance of the method predictions over a range of edge cutoffs can be evaluated using method_performance_edge_range(), which outputs a figure. This is used as a comparison to the pairwise_correlation() method.</p>
<pre><code>c.method_performance_edge_range(edge_range=None, plot=True)</code></pre>
<h4 id="consensus-network">Consensus network</h4>
<p>ComHub also contains a function for combining predicted networks into a consensus network. The methodology behind the consensus network is taken from marbach et al. 2012. The consensus network is saved as “/path/to/comhub/results/{network_name}/consensus_network.csv”</p>
<pre><code>consensus_net = c.consensus_network(edge_cutoff=100000, network_files=None, save_file=True, output_name=None)</code></pre>
<h4 id="documentation">Documentation</h4>
<p>Some simple documentation for each function is available using:</p>
<pre><code>c.{function}.__doc__</code></pre>
