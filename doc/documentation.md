# ComHub Documentation

## Table of Contents
- [Usage](#usage)
  - [Demo usage](#demo-usage)
  - [Available ComHub methods](#available-comhub-methods)
    - [Setup ComHub](#setup-comhub)
    - [Infer gene regulatory networks](#infer-gene-regulatory-networks)
    - [Search for a edge threshold](#search-for-a-edge-threshold)
    - [Making a community hub prediction](#making-a-community-hub-prediction)
    - [Identifying hubs](#identifying-hubs)
    - [Performance assessment](#performance-assessment)
    - [Documentation](#documentation)
- [License](#license)
- [References](#references)

## Usage
### Demo usage
Install comhub as stated in [installation instructions](https://gitlab.com/Gustafsson-lab/comhub/tree/master/doc/).
Place yourself in comhub folder and start Python.  

Run comhub on test data:
```
from bin.comhub import comhub
c = comhub(network_name='test')
c.run_methods(bootstrap=10) #To reduce time of Elastic Net bootstrap is set to 10.
edge_cutoff = c.pairwise_correlation()
tf_outdegree = c.get_tf_outdegree(edge_cutoff)
community = c.community(tf_outdegree)
```
The results are saved in the folder "/path/to/comhub/results/test/".

### Available ComHub methods
ComHub is implemented as a Python class. ComHub requires two input files containing gene expression data and a list of possible transcription factors.

#### Setup comhub
Import ComHub with:
```
from bin.comhub import comhub
```
Create a ComHub object with:
```
c = comhub(network_name='test', methods=['aracne', 'clr_R', 'pcc', 'elasticnet_bootstrap', 'tigress_R', 'genie3'], expression_data=None, transcription_factors=None, gold_standard=None)
```
By default ComHub inputs expression data named "/path/to/comhub/data/{network_name}_expression_data.tsv" and possible regulators named "/path/to/comhub/data/{network_name}_transcription_factors.tsv". It is possible to submit any files with expression data and transcription factors. The expression data need to be tab seperated where columns are genes and rows are samples. There should be column names but no row names.
Example of expression data format:
```
gene1	gene2	gene3
0.62	0.25	0.35
0.51	0.21	0.40
0.46	0.15	0.32
```
Example of transcription factors format:
```
tf1
tf2
tf3
```

The parameter methods is used to set which network inference methods that should be used for predicting hubs. By default aracne, clr_R, pcc, elasticnet_bootstrap, tigress_R, and genie3 are used. It is possible to provide networks inferred by your own methods. Then you set the parameter methods to a list with the methods you want to use, including the name of your method.

#### Infer gene regulatory networks
The network inference methods are applied on the ComHub object. All predicted networks will be saved as "/path/to/comhub/networks/{network_name}/{method}_network.csv".
Run all network inference methods stated when making the ComHub object with:
```
c.run_methods(network_cutoff=100000, nstepsLARS=5, matlab=False, bootstrap=100, parallel=True)
```
This is only recommended on very small data sets. For most data sets it's better to run the methods one by one.

It is possible to run all network inference methods separately with:
```
net = c.aracne(network_cutoff=100000)	
net = c.pcc(network_cutoff=100000)
net = c.genie3(network_cutoff=100000)
```
CLR and TIGRESS are both available as a R-version and a MATLAB-version.  
Run CLR and TIGRESS in R with:
```
net = c.clr_R(network_cutoff=100000)
net = c.tigress_R(network_cutoff=100000, nstepsLARS=5, alpha=0.2, nsplit=100)
```
For the MATLAB-version of CLR and TIGRESS it is possible to choose if the method should be run in MATLAB or Octave. By default the methods will run in Octave. However, for faster performance it is recommended to run in MATLAB.
```
net = c.clr(network_cutoff=100000, matlab=False)
net = c.tigress(network_cutoff=100000, matlab=False)
```
For bootstrap Elastic Net it is posible to choose how many bootstraps that should be run and if the bootstraps should be run in parallell.
```
net = c.elasticnet_bootstrap(bootstrap=100, cv=3, network_cutoff=100000, large_dataset=False, matrix_name='')
```
For large data sets, you might prefer to do several runs of Elastic net only with one or a few bootstraps and save the resulting matrices. When all runs are done you can combine the produced matrices into a network.

Running ElasticNet with large_dataset=True will save the resulting matrix into the folder "/path/to/comhub/networks/{network_name}/en_matrices". The method elasticnet_combine_matrices() can then be used to combine all the matrices saved in that folder. You can also submit all the filepaths in a list.
```
for i in range(100):
    c.elasticnet_bootstrap(bootstrap=1, large_dataset=True, matrix_name=str(i))
net = c.elasticnet_combine_matrices(files=None, network_cutoff=100000)
```
The last method implemented in ComHub is bootstrap LASSO, which can be run in the same way as Elastic Net. For smaller data sets use:
```
net = c.elasticnet_bootstrap(bootstrap=100, cv=3, network_cutoff=100000, large_dataset=False, matrix_name='')
```
For large data sets you can use: 
```
for i in range(100):
    c.elasticnet_bootstrap(bootstrap=1, large_dataset=True, matrix_name=str(i))
net = c.elasticnet_combine_matrices(files=None, network_cutoff=100000)
```

#### Search for a edge threshold
An edge threshold is identified using the method pairwise_correlation(). pairwise_correlation() reads predicted networks from the network inference methods stated when making the comhub object. pairwise_correlation() looks after files named "/path/to/comhub/networks/{network_name}/{method}_network.tsv". The function will generate a figure if plot=True that will be saved as "/path/to/comhub/results/{network_name}/pairwise_correlation.png". It is possible to generate several figures for different edge ranges by setting fig_name='your_name' that will be added to the figure name as pairwise_correlation{your_name}.png. 
```
edge_cutoff = c.pairwise_correlation(edge_range=[500, 1000, 2000, 3000, 4000, 5000, 7000, 10000, 15000, 20000, 50000, 80000, 100000], plot=True, fig_name='', network_files=None)
```
The parameter edge range sets which edge thresholds to run pairwise correlation for. The method will output the optimal one of these edge thresholds.
Using the parameter network_files you can input a list with filepaths to any network files you want to use. By default the network files "/path/to/comhub/networks/{network_name}/{method}_network.tsv" will be used.


#### Making a community hub prediction
The regulator outdegrees of each predicted network is calcualted using the method get_tf_outdegree(). By default get_tf_outdegree() inputs predicted networks from the network inference methods stated when making the comhub object. get_tf_outdegree() looks in the folder "/path/to/comhub/networks/{network_name}/" after predicted networks named as "{method}_network.csv". It is possible to input your own network files as a list by setting network_files=['/path/to/network1','path/to/network2',...,'/path/to/networkn']
```
tf_outdegree = c.get_tf_outdegree(edge_cutoff=100000, network_files=None)
```
The community regulator outdegree is calculated using community(). community() saves the result as "/path/to/comhub/results/{network_name}/community.csv". It is possible to add an additional name to the output with output_name='your_name', to allow communities being saved for different edge thresholds. community() outputs a list of regulators ranked on outdegree.
```
community = c.community(tf_outdegree, save_csv=True, output_name='')
```

#### Identifying hubs
Hubs can  be identified from the community using the method hubs(). hubs() by default identifies the regulators standing for 10% of the total interactions in the network. hubs() saves the result as "/path/to/comhub/results/{network_name}/hubs.csv". It is possible to add an additional name to the output with output_name='your_name'.
```
hubs = c.hubs(community, percentage_interactions=0.1, save_csv=True, output_name='')
```

#### Performance assessment
It is possible to assess the performance of ComHub and the network inference methods if a gold standard network, named "/path/to/comhub/data/{network_name}_gold_network.tsv", is available. The file path to a gold standard can also be provided when creating the ComHub object by changing the parameter gold_standard.  
The performance of ComHub is calculated using the method community_performance(). community_performance() calculates the pearson correlation coefficient between regulator outdegrees in ComHub prediction and in the gold standard.
```
pcc, pval = c.community_performance(community)
```
The performance of ComHub compared to the network inference methods is calculated using the method method_performance(). The function can be run for different edge thresholds. The result is saved as a figure named "/path/to/comhub/results/{network_name}/method_performance_{edge_threshold}.png".
```
performance = c.method_performance(edge_cutoff, plot=True)
```
The average performance of the method predictions over a range of edge cutoffs can be evaluated using method_performance_edge_range(), which outputs a figure. This is used as a comparison to the pairwise_correlation() method. 
```
c.method_performance_edge_range(edge_range=None, plot=True)
```

#### Consensus network
ComHub also contains a function for combining predicted networks into a consensus network. The methodology behind the consensus network is taken from marbach et al. 2012. 
The consensus network is saved as "/path/to/comhub/results/{network_name}/consensus_network.csv"
```
consensus_net = c.consensus_network(edge_cutoff=100000, network_files=None, save_file=True, output_name=None)
```

#### Documentation
Some simple documentation for each function is available using:
```
c.{function}.__doc__
```
