# ComHub Installation
First you should clone the ComHub GitLab repository:
```
git clone https://gitlab.com/Gustafsson-lab/comhub.git
```
When using ComHub you need the have this repository as your working directory.  

ComHub has some Minimum dependencies and some Additional dependencies. If ComHub only is used on a set of user-provided GRN predictions, it is only needed to install the minimum dependencies. It is also possible to run the method absolute value of Pearson correlation coefficient with only the minimum dependencies. Additional dependencies need to be installed if using the network inference methods: ARACNE, TIGRESS, CLR, and bootstrap Elastic Net. The network inference methods GENIE3, CLR(matlab), and TIGRESS(matlab) calls other scripts, which are downloaded as a part of ComHub. Note that the methods CLR and TIGRESS both has R-versions and MATLAB-versions available.  

You can either install the ComHub dependencies using pip or conda. To use conda you first need to download [Anaconda](https://www.anaconda.com/distribution/) with python 3. With conda you can create an evironment for comhub with:
```
conda create --name comhub
conda activate comhub
```

If you are not using conda you first need to install Python from https://www.python.org/downloads/.  

### Minimum dependencies
```
pip install numpy pandas scipy matplotlib seaborn
```
or
```
conda install python # if not already installed
conda install numpy pandas scipy matplotlib seaborn
```
Versions used when testing comhub:
- Python 3.6.9
- numpy 1.16.4
- pandas 0.25.1
- scipy 1.3.1
- matplotlib 3.1.1
- seaborn 0.9.0

### Additional dependencies
To use bootstrap Elastic Net and bootstrap LASSO you need the python package scikit-learn and joblib:
```
pip install scikit-learn
pip install joblib #Not needed if using Parallell=False 
```
or
```
conda install scikit-learn
conda install joblib #Not needed if using Parallell=False 
```
The method CLR also requires scikit-learn to be installed.

The network inference methods ARACNE, CLR, and TIGRESS are written in R and therefore requires some R-packages. It is possible to install these R packages using conda but I prefer to have R separately installed.

Install R from https://www.r-project.org/.

Install rpy2 with:
```
pip install rpy2
```
The methods ARACNE and CLR uses the R package minet.
Open R in your terminal and install the package minet with:
```
if (!requireNamespace("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("minet")
```
Optionally you can use conda:
```
conda install -c r r
conda install -c r rpy2
conda install -c bioconda bioconductor-minet
```
[TIGRESS(R)](https://github.com/jpvert/tigress) can be installed from R with:
```
install.packages('devtools')
library(devtools)
install_github("jpvert/tigress")
```

It is possile to use the MATLAB version of TIGRESS and CLR. Then you should install Octave or MATLAB. These packages can be installed using conda.
Octave:
```
conda install -c conda-forge octave
conda install -c conda-forge oct2py
```
MATLAB:
- Download [MATLAB](https://se.mathworks.com/products/get-matlab.html?s_tid=gn_getml)
- Install [MATLAB engine for python](https://se.mathworks.com/help/matlab/matlab-engine-for-python.html) with:
```
cd "/path/to/MATLAB/R2019a/extern/engines/python" # often "/usr/local/MATLAB/R2019a/extern/engines/python"
sudo python3 setup.py install --prefix="/path/to/anaconda3/envs/comhub/"
```

Versions used when testing comhub:
- scikit-learn 0.21.2
- joblib 0.13.2
- R 3.5.1
- rpy2 2.9.4
- minet 3.4.0
- GNU Octave 4.2.2
- oct2py 5.0.4
- MATLAB R2019a 

